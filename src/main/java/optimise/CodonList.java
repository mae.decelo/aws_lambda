package optimise;

/**
 * The CodonList enum describes the amino acid produced by each codon sequence.
 * @author: Anna Mae Decelo, Student ID 220226741, adecelo@myune.edu.au
 */
public enum CodonList {
    AAA("K"),
    AAC("N"),
    AAG("K"),
    AAT("N"),
    ACA("T"),
    ACC("T"),
    ACG("T"),
    ACT("T"),
    AGA("R"),
    AGC("S"),
    AGG("R"),
    AGT("S"),
    ATA("I"),
    ATC("I"),
    ATG("M"),
    ATT("I"),
    CAA("Q"),
    CAC("H"),
    CAG	("Q"),
    CAT	("H"),
    CCA	("P"),
    CCC	("P"),
    CCG	("P"),
    CCT	("P"),
    CGA	("R"),
    CGC	("R"),
    CGG	("R"),
    CGT	("R"),
    CTA	("L"),
    CTC	("L"),
    CTG	("L"),
    CTT	("L"),
    GAA	("E"),
    GAC	("D"),
    GAG	("E"),
    GAT	("D"),
    GCA	("A"),
    GCC	("A"),
    GCG	("A"),
    GCT	("A"),
    GGA	("G"),
    GGC	("G"),
    GGG	("G"),
    GGT	("G"),
    GTA	("V"),
    GTC	("V"),
    GTG	("V"),
    GTT	("V"),
    TAA	("s"),
    TAC	("Y"),
    TAG	("s"),
    TAT	("Y"),
    TCA	("S"),
    TCC	("S"),
    TCG	("S"),
    TCT	("S"),
    TGA	("s"),
    TGC	("C"),
    TGG	("W"),
    TGT	("C"),
    TTA	("L"),
    TTC	("F"),
    TTG	("L"),
    TTT	("F");

    private final String aminoAcid;

    CodonList(String aminoAcid) {
        this.aminoAcid = aminoAcid;
    }

    /**
     * Function to retrieve the amino acid produced by a codon.
     * @return - the amino acid String.
     */
    public String getAminoAcid() {
        return aminoAcid;
    }
}
