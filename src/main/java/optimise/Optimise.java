package optimise;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

/**
 * The Optimise class implements the handleRequest method of the RequestStreamHandler.
 * The function in this class executes an AWS Lambda function named "optimise", which
 * can be invoked locally using the serverless framework. This Lambda function optimises
 * codon input in the JSON format with the key "Codon".
 * @author: Anna Mae Decelo, Student ID 220226741, adecelo@myune.edu.au
 */
public class Optimise implements RequestStreamHandler {

    private final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Function that implements the handleRequest handler of the AWS Lambda function "optimise".
     * @param inputStream - the data input as a stream of bytes.
     * @param outputStream - the stream to write the optimised result to.
     * @param context - information from the Lambda execution environment.
     * @throws IOException - exception thrown if the ObjectMapper cannot read from the InputStream,
     *                  if the ObjectMapper cannot turn the Codon object into String, or if the
     *                  OutputStreamWriter has been closed and write() or flush() is called.
     */
    @Override
    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context)
             throws IOException {
        context.getLogger().log("Input: " + inputStream);
        context.getLogger().log("Output: " + outputStream);
        Codon codonInput = objectMapper.readValue(inputStream, Codon.class);
        codonInput.processCodon();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStreamWriter.write(objectMapper.writeValueAsString(codonInput));
        outputStreamWriter.close();
    }
}
