package optimise;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

/**
 * The Codon class defines the functions that enable the Optimise Class to
 * return the optimised version of the RNA inputted when the Lambda function
 * "optimise" is invoked.
 * @author: Anna Mae Decelo, Student ID 220226741, adecelo@myune.edu.au
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Codon {
    @JsonProperty("Codon")
    String codon;

    /**ArrayLists used in processing the codon input*/
    ArrayList<String> inputArray = new ArrayList<>();
    ArrayList<String> optimisedArray = new ArrayList<>();
    ArrayList<String> matchingCodonsArray = new ArrayList<>();
    Map<String, Integer> characterCountArray = new HashMap<>();

    /**
     * Function that processes the codon value of the JSON
     * property "Codon" for optimisation.
     */
    public void processCodon() {
        optimisedArray.clear();
        inputArray.clear();

        // Replace 'U' bases with 'T', remove all whitespaces and turn codon input into uppercase
        String clientInputSubstituted = codon.toUpperCase().replaceAll("U", "T")
                .replaceAll("\\s", "");

        // If input is only one codon and already contains 3 'G'/'C' characters, return the input
        if (clientInputSubstituted.length() == 3 && countGCCharacters(clientInputSubstituted) == 3) {
            codon = clientInputSubstituted.toUpperCase();

        // Process the entered codon for optimisation if the length and characters are valid
        } else if (clientInputSubstituted.length() % 3 == 0 && clientInputSubstituted.matches("[aAcCgGtT]+")) {
            ArrayList<String> resultArray = getOptimisedArray(clientInputSubstituted);

            // If the input is already optimal, return the input
            if (printOptimisedCodon(resultArray).equalsIgnoreCase(clientInputSubstituted)) {
                codon = clientInputSubstituted.toUpperCase();
            }

            // Return the optimised codon
            else {
                codon = printOptimisedCodon(resultArray);
            }

        // Replace all 'T' bases in the 5'-UTR with 'Ψ' bases
        } else if (clientInputSubstituted.equalsIgnoreCase("GAATAAACTAGTATTCTTCTGGTCCCCACAGACTCAGAGAGAACCCGCCACC")) {
            codon = codon.replaceAll("T", "Ψ");

        // Return error if input is invalid
        } else if (!clientInputSubstituted.matches("[aAcCgGtT]+") || clientInputSubstituted.length() % 3 != 0) {
            codon = "ERROR, INVALID ENTRY.";
        }
    }

    /**
     * Function to process codons as an array of the input.
     * @param input - String entered as input from the client.
     * @return - an ArrayList containing the optimised codons from the input.
     */
    public ArrayList<String> getOptimisedArray(String input) {
        // Split input into lengths of 3 bases
        String[] str = input.split("(?<=\\G.{3})");
        Collections.addAll(inputArray, str);

        // Optimise each codon entered from client
        for (String i : inputArray) {
            String result = optimiseCodon(i);
            optimisedArray.add(result);
        }

        // Replace 'AAGGTC' codon sequences with 'CCTCCT'
        if (optimisedArray.toString().contains("AAG, GTC")) {
            ListIterator<String> iterator = optimisedArray.listIterator();
            while (iterator.hasNext()) {
                String next = iterator.next();
                if (next.equals("AAG")) {
                    iterator.set("CCT");
                } else if (next.equals("GTC")) {
                    iterator.set("CCT");
                }
            }
        }
        return optimisedArray;
    }

    /**
     * Function to optimise each individual codon according to the optimisation rules.
     * @param unoptimisedCodon - String of a single codon.
     * @return - the optimal codon.
     */
    public String optimiseCodon(String unoptimisedCodon) {
        String optimisedCodon = null;

        // Return original unoptimisedCodon if 'G'/'C' codon character count is already 3
        if (countGCCharacters(unoptimisedCodon) == 3) {
            optimisedCodon = unoptimisedCodon;

            // Return codon CCT if the unoptimisedCodon is CCA
        } else if (unoptimisedCodon.equalsIgnoreCase("CCA")) {
            optimisedCodon = "CCT";

            // Return stop codon TGATGA if the unoptimisedCodon is the stop codon TAA
        } else if (unoptimisedCodon.equalsIgnoreCase("TAA")) {
            optimisedCodon = "TGATGA";

            // Optimise the unoptimisedCodon according to the number of 'G'/'C' characters
        } else {
            CodonList codonList = CodonList.valueOf(unoptimisedCodon.toUpperCase());
            String aminoAcid = codonList.getAminoAcid();

            // Get list of codons with 'G'/'C' that produces the same amino acid
            for (CodonList i : CodonList.values()) {
                String matchingAminoAcid = i.getAminoAcid();
                if (matchingAminoAcid.equals(aminoAcid)) {
                    if (i.toString().indexOf('G') != -1 || i.toString().indexOf('C') != -1) {
                        matchingCodonsArray.add(i.toString());
                    }
                }
            }

            // Find the codon with the most number of 'G'/'C' characters
            for (String value : matchingCodonsArray) {
                characterCountArray.put(value, countGCCharacters(value));
                Map.Entry<String, Integer> maxEntry = null;

                for (Map.Entry<String, Integer> entry : characterCountArray.entrySet()) {
                    if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                        maxEntry = entry;
                    }

                    // If original unoptimisedCodon has more or equal number of 'G'/'C' return the original unoptimisedCodon
                    if (countGCCharacters(unoptimisedCodon) >= maxEntry.getValue()) {
                        optimisedCodon = unoptimisedCodon;

                        // Else return codon with more 'G'/'C' characters than unoptimisedCodon
                    } else {
                        optimisedCodon = maxEntry.getKey();
                    }
                }
            }
        }
        matchingCodonsArray.clear();
        characterCountArray.clear();
        return optimisedCodon;
    }

    /**
     * Function that counts the number of 'G' and 'C' characters in a String.
     * @param codon - String of a codon.
     * @return - the int of the number of occurrences of the 'G' and 'C' characters in a codon.
     */
    public int countGCCharacters (String codon) {
        int count = 0;
        for (int i = 0; i < codon.length(); i++) {
            if (codon.charAt(i) == 'G' || codon.charAt(i) == 'g' ||
                    codon.charAt(i) == 'C' || codon.charAt(i) == 'c') {
                count++;
            }
        }
        return count;
    }

    /**
     * Function to print an ArrayList as a single String.
     * @param array - an ArrayList of codons.
     * @return - the list of codons as a String.
     */
    public String printOptimisedCodon (ArrayList<String> array) {
        StringBuilder builder = new StringBuilder();
        for (String value : array) {
            builder.append(value);
        }
        return builder.toString();
    }
}
