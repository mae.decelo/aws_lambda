# Covid Vaccine RNA Optimisation

---------------------------------------------

This program utilises AWS to implement a serverless application by utilising
an AWS Lambda function called `optimise`, which carries out the optimisation of
RNA, aiding in the creation of a better Covid-19 vaccine.

The [article on reverse engineering the SARS-CoV-2 Vaccine by Bert
Hubert](https://berthub.eu/articles/posts/reverse-engineering-source-code-of-the-biontech-pfizer-vaccine)
was the basis for the theory behind the optimisation rules implemented in this program.

## Usage - Invoking the function locally on turing.une.edu.au

---------------------------------------------

### Configuring settings and installing serverless

Invoking the Lambda function locally on Turing requires npm to first be installed. For npm to be
installed on Turing, the proxy settings for npm must first be configured, where `username` is the une.edu.au
user account and the `password` is the associated pass phrase for the account:

```shell
npm config set proxy http://username:password@proxy.une.edu.au:8080
npm config set https-proxy http://username:password@proxy.une.edu.au:8080
```

Afterwards, npm can be installed with the command:

```shell
npm install serverless
```

The proxy for maven must also be configured in the `settings.xml` file in the directory
/home/username/.m2/. If the `settings.xml` file is not found in this directory, the settings
can be copied from the `settings.xml` file found in the folder /usr/share/maven/conf/.

Within `settings.xml` in /home/username/.m2/, under the `<proxies>` setting, add the following
configurations, while also changing `username` and `passphrase` to your personal configurations:

```xml
<proxy>
  <host>proxy.une.edu.au</host>
  <port>8080</port>
  <username>username</username>
  <password>passphrase</password>
</proxy>
```

Afterwards, run the following code in the directory
/home/username/node_modules/serverless/lib/plugins/aws/invokeLocal/runtimeWrappers/java/:

```shell
mvn package
```

---------------------------------------------

### Invoking the function with data 

After setting the configurations above and installing serverless, the function can be implemented 
locally using the following command. Note that this command must be invoked in the root directory 
of this project, `Assessment6`, where the `serverless.yml` file is located:

```shell
cd Assessment6
npx serverless invoke local -f optimise -d '{"Codon":"AAAAACACA"}'
```

Event objects passed through the `optimise` function must be formatted as a JSON String
within single quotation marks with the input codon as the value of the key "Codon",
i.e. `{"Codon":"<codon to be optimised>"}`:

```shell
'{"Codon":"AAAAACACA"}'
```

The function will then return the optimised codon to the terminal:

```shell
{"Codon":"AAGAACACC"}
```

---------------------------------------------

### Invoking the function with a filepath 

The function can also process information from .txt or .json files. To
invoke the function locally with input from a file, run the following command
including the variable `-p` or `--path` and the absolute path of the file:

```shell
cd Assessment6
npx serverless invoke local -f optimise -p /home/username/Assessment6/src/main/resources/file.txt
```

Note that the file contents must still be formatted as a JSON String with the codon
to be optimised set as the value of the key "Codon", as was outlined earlier. 

## Author

Anna Mae Decelo ([adecelo@myune.edu.au](mailto:adecelo@myune.edu.au), student ID 220226741)